const express = require('express');
const cors = require('express-cors');
const faker = require('faker');
const app = express();
const session = require('express-session');
const cookieParser = require('cookie-parser');
app.use(
	session({
		name: 'session-name',
		secret: 'my_session_secret',
		resave: true,
		saveUninitialized: false,
		cookie: { maxAge: 60 * 1000, httpOnly: true },
	})
);
const generateData = () => {
	const eventList = [];
	for (let i = 0; i < 10; i++) {
		const event = {};
		event.username = faker.name.findName();
		event.channel = faker.commerce.productMaterial();
		event.time = faker.date
			.past()
			.getTime()
			.toString();
		event.content = faker.lorem.sentences();
		event.goingNum = 3;
		event.likesNum = 3;
		event.img = faker.image.image();
		event.title = faker.lorem.sentence();
		event.avatar = faker.internet.avatar();
		event.going = false;
		event.liked = false;
		eventList.push(event);
	}
	return eventList;
};

const generateDetailData = () => {
	const detailData = {};
	detailData.goingNum = 5;
	detailData.likeNum = 5;
	detailData.content = faker.lorem.paragraphs();

	const images = [];
	const goingAvatars = [];
	const likeAvatars = [];
	const comments = [];
	for (let i = 0; i < 5; i++) {
		images.push(faker.image.image());
		goingAvatars.push(faker.internet.avatar());
		likeAvatars.push(faker.internet.avatar());
		const comment = {
			avatar: faker.internet.avatar(),
			content: faker.lorem.sentences(),
			username: faker.name.findName(),
		};
		comments.push(comment);
	}
	detailData.comments = comments;
	detailData.images = images;
	detailData.goingAvatars = goingAvatars;
	detailData.likeAvatars = goingAvatars;
	return detailData;
};

app.post('/login', (req, res) => {
	const user = {};
	user.avatar = faker.image.avatar();
	user.email = faker.internet.email();
	user.username = faker.name.findName();
	req.session.logined = true;
	res.send({ msg: 'success', user });
});

app.get('/detail', (req, res) => {
	const detailData = generateDetailData();
	res.send(detailData);
});

app.get('/eventList', (req, res) => {
	const eventList = generateData();
	res.send(eventList);
});

app.get('/verify', (req, res) => {
	if (req.session.logined) {
		const user = {};
		user.avatar = faker.image.avatar();
		user.email = faker.internet.email();
		user.username = faker.name.findName();
		res.send({ logined: true, user });
	} else {
		res.send({ logined: false });
	}
});

app.listen(3000, () => console.log('server start'));
