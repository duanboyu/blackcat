const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
	mode: 'development',
	entry: './src/index.tsx',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.scss', '.js', '.json'],
	},
	module: {
		rules: [
			{
				test: /\.jsx?/i,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-react'],
						},
					},
					{
						loader: 'eslint-loader',
					},
				],
			},
			{
				test: /\.css$/i,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'imgs/',
					},
				},
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/,
				use: {
					loader: 'url-loader',
					options: {
						outputPath: 'imgs/',
						publicPath: 'dist/imgs',
					},
				},
			},
			{
				test: /\.scss$/i,
				use: [
					{ loader: 'style-loader' },
					{
						loader: 'typings-for-css-modules-loader',
						options: {
							modules: true,
							namedExport: true,
							camelCase: true,
						},
					},

					{ loader: 'sass-loader' },
				],
			},
			{ test: /\.tsx?$/, loader: 'ts-loader' },
		],
	},
	devtool: 'source-map',
	plugins: [
		new HtmlWebpackPlugin({
			template: 'src/index.html',
		}),
	],
	devServer: {
		historyApiFallback: true,
		contentBase: path.resolve(__dirname, './dist'),
		compress: true,
		port: 5555,
		open: true,
		proxy: {
			'/api': {
				target: 'http://localhost:3000/',
				pathRewrite: { 'api/': '' },
				changeOrigin: true, // target是域名的话，需要这个参数，
				secure: false, // 设置支持https协议的代理
			},
		},
	},
};
