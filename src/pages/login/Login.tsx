import * as React from "react";
import { useState } from "react";
import classnames from "classnames";
import * as LoginStyle from "./Login.scss";
import { connect } from "react-redux";
import { loginSubmit } from "../../store/login/action";
import { Redirect } from "react-router-dom";

const Login = props => {
	const { dispatch, redirect, location } = props;

	if (redirect && redirect != location.pathname) {
		return <Redirect to={redirect} />;
	}
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");

	const setText = (e: any, type: string): void => {
		if (type == "username") {
			setUsername(e.target.value);
		}
		if (type == "password") {
			setPassword(e.target.value);
		}
	};

	const submit = (): void => {
		dispatch(loginSubmit(username, password));
	};
	return (
		<div className={LoginStyle.background}>
			<div className={LoginStyle.login}>
				<div className={LoginStyle.header}>
					<span className={LoginStyle.slogan}>
						FIND THE MOST LOVED ACTIVITIES
					</span>
					<span className={LoginStyle.name}>BLACK CAT</span>
					<div className={LoginStyle.svgWrapper}>
						<svg
							height="56"
							width="48"
							fill="#d5ef7f"
							id="Layer_1"
							data-name="Layer 1"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 32 32"
						>
							<polygon points="26.47 14.44 23.07 19.93 23.07 27.38 25.83 29.84 19.2 29.84 21.89 27.36 21.89 19.72 15.69 10.95 19.62 10.95 21.48 9.19 18.18 4.17 14.73 3.14 15.15 -0.03 9.92 4.17 2.83 17.38 7.78 28.12 5.51 30.53 5.51 31.97 9.26 31.97 10.18 31.48 10.93 31.97 29.94 31.97 29.94 30.25 25.68 25.99 25.68 20.55 27.96 16.84 28.78 16.84 29.2 20.08 30.4 20.08 30.71 14.44 26.47 14.44" />
						</svg>
					</div>
				</div>
				<div className={LoginStyle.input}>
					<div
						className={classnames(
							LoginStyle.inputWrapper,
							LoginStyle.inputWrapperUsername
						)}
					>
						<svg
							fill="#D3C1E5"
							height="1rem"
							width="1rem"
							id="Layer_1"
							data-name="Layer 1"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 32 32"
						>
							<title>user</title>
							<path d="M16,1.94A14.06,14.06,0,1,0,30.06,16,14.06,14.06,0,0,0,16,1.94Zm0,4.22a4.22,4.22,0,1,1-4.22,4.22A4.21,4.21,0,0,1,16,6.16Zm0,20a10.12,10.12,0,0,1-8.43-4.53c0-2.8,5.62-4.33,8.43-4.33s8.39,1.53,8.43,4.33A10.12,10.12,0,0,1,16,26.12Z" />
						</svg>
						<input
							type="email"
							placeholder="username"
							onChange={e => setText(e, "username")}
						/>
					</div>

					<div
						className={classnames(
							LoginStyle.inputWrapper,
							LoginStyle.inputWrapperPassword
						)}
					>
						<svg
							height="1rem"
							width="1rem"
							fill="#D3C1E5"
							id="Layer_1"
							data-name="Layer 1"
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 32 32"
						>
							<title>password</title>
							<path d="M13.33,30.67h5.33V28H24V22.67H18.67v-5.8a8,8,0,1,0-5.33,0v13.8ZM16,12a2.67,2.67,0,1,1,2.67-2.67A2.67,2.67,0,0,1,16,12Z" />
						</svg>
						<input
							type="password"
							placeholder="password"
							onChange={e => setText(e, "password")}
						/>
					</div>
				</div>
				<div className={LoginStyle.bottom} onClick={() => submit()}>
					SIGN IN
				</div>
			</div>
		</div>
	);
};

export default connect(
	function mapStateToProps(state: any) {
		return { redirect: state.login.redirect };
	},
	function mapStateToProps(dispatch) {
		return { dispatch };
	}
)(Login);
