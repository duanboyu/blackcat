import * as React from "react";
import { generateDateString } from "../../utils";
import Header from "../../components/header/Header";
import { connect } from "react-redux";
import { useCallback, useEffect, useMemo, useState } from "react";
import * as style from "./style.scss";
import BottomBar from "./components/bottomBar/bottomBar";
import Comment from "./components/comment/comment";
import {
	fetchDetailData,
	setShowAll,
	setCurrentSelect,
	sendComment
} from "../../store/detail/action";
import { setGoing, setLiked } from "../../store/home/action";
import { bindActionCreators } from "redux";
import CommentInput from "./components/commentInput/commentInput";
import Toast from "./components/toast/toast";
import { Redirect } from "react-router-dom";
import { setRedirect } from "../../store/login/action";

function index(props) {
	const {
		avatar,
		dispatch,
		currentIndex,
		eventList,
		images,
		goingAvatars,
		likeAvatars,
		goingNum,
		likeNum,
		content,
		showAll,
		currentSelect,
		comments,
		redirect,
		location
	} = props;

	if (redirect && redirect != location.pathname) {
		return <Redirect to={redirect} />;
	}
	const eventInfo = eventList[currentIndex];
	if (!eventInfo) {
		return null;
	}
	const { channel, username, title, time, liked, going } = eventInfo;

	useEffect(() => {
		dispatch(fetchDetailData());
		return () => {
			dispatch(setShowAll(false));
			dispatch(setCurrentSelect("Details"));
		};
	}, []);
	useEffect(() => {
		if (currentSelect == "Comments") {
			document.getElementById("bottom").scrollIntoView();
		}
	});

	const [showComment, setShowComment] = useState(false);
	const [showToast, setShowToast] = useState(false);
	const [latestComment, setLatestComment] = useState("");

	const onSend = useCallback((content: string) => {
		dispatch(sendComment({ content, username, avatar }));
		dispatch(setCurrentSelect("Comments"));
		setLatestComment(content);
	}, []);
	const bottomCbs = useMemo(
		() =>
			bindActionCreators(
				{
					onGoing: setGoing,
					onLiked: setLiked
				},
				dispatch
			),
		[]
	);
	const goBack = useCallback(() => {
		dispatch(setRedirect("/home"));
	}, []);
	const toMe = useCallback(() => {
		dispatch(setRedirect("/me"));
	}, []);

	const contentJSX = (function generateJSX() {
		if (showAll) {
			return <div className={style.showAll}>{content}</div>;
		} else {
			return (
				<div className={style.contentWrapper}>
					<div className={style.content}>
						<p>{content}</p>
					</div>
					<div className={style.mask} />
					<div
						onClick={() => dispatch(setShowAll(true))}
						className={style.viewAll}
					>
						<span>VIEW ALL</span>
					</div>
				</div>
			);
		}
	})();

	return (
		<div>
			<div className={style.header}>
				<Header
					rightCallback={toMe}
					showSearch={false}
					avatar={avatar}
					leftCallback={goBack}
				/>
			</div>
			<div className={style.basicInfo}>
				<div className={style.channelName}>{channel}</div>
				<div className={style.title}>{title}</div>
				<div className={style.userInfo}>
					<div
						className={style.userInfoLeft}
						style={{ backgroundImage: `url('${eventInfo.avatar}')` }}
					/>
					<div className={style.userInfoRight}>
						<span className={style.username}>{username}</span>
						<span className={style.publishTime}>
							Published{" "}
							{Math.floor(
								(new Date().getTime() - Number(time)) / (24 * 3600 * 1000)
							) +
								1 +
								"	days ago"}
						</span>
					</div>
				</div>
			</div>
			<div className={style.pick}>
				<div className={style.infoWrapper}>
					<div className={style.leftDivide} />
					<div className={style.rightDivide} />
					<div
						onClick={() => {
							dispatch(setCurrentSelect("Details"));
							window.document.documentElement.scrollTop = 0;
						}}
						className={style.oneInfo}
					>
						<div
							className={
								currentSelect == "Details"
									? style.greenDetail
									: style.brownDetail
							}
						/>
						<div
							className={
								currentSelect == "Details" ? style.greenText : style.brownText
							}
						>
							Details
						</div>
					</div>
					<div
						onClick={() => {
							dispatch(setCurrentSelect("Participants"));
							document.getElementById("pa").scrollIntoView();
							document.documentElement.scrollTop =
								document.documentElement.scrollTop - 88;
						}}
						className={style.oneInfo}
					>
						<div
							className={
								currentSelect == "Participants"
									? style.greenPeople
									: style.brownPeople
							}
						/>
						<div
							className={
								currentSelect == "Participants"
									? style.greenText
									: style.brownText
							}
						>
							Participants
						</div>
					</div>
					<div
						onClick={() => {
							dispatch(setCurrentSelect("Comments"));
							document.getElementById("bottom").scrollIntoView();
						}}
						className={style.oneInfo}
					>
						<div
							className={
								currentSelect == "Comments"
									? style.greenComment
									: style.brownComment
							}
						/>
						<div
							className={
								currentSelect == "Comments" ? style.greenText : style.brownText
							}
						>
							Comments
						</div>
					</div>
				</div>
			</div>
			{showToast ? <Toast content={latestComment} /> : null}
			<div id="detail" className={style.imageContainer}>
				{images.map((image, index) => {
					return (
						<div
							key={index}
							className={style.image}
							style={{ backgroundImage: `url('${image}')` }}
						/>
					);
				})}
				<div className={style.rightPadding} />
			</div>
			{contentJSX}
			<div className={style.timeInfo}>
				<div className={style.when}>
					<div className={style.shortline} />
					<span>When</span>
				</div>
				<div className={style.timemiddle}>
					<div className={style.timeleft}>
						<div className={style.startTime}>
							<div className={style.startArrow} />
							<span>{generateDateString(Number(time))}</span>
						</div>
						<span className={style.timeText}>
							8:30 <span style={{ fontSize: 12 }}>am</span>
						</span>
					</div>

					<div className={style.timeright}>
						<div className={style.startTime}>
							<div className={style.endArrow} />
							<span>{generateDateString(Number(time))}</span>
						</div>
					</div>
				</div>
			</div>
			<div className={style.addressInfo}>
				<div className={style.when}>
					<div className={style.shortline} />
					<span>Where</span>
				</div>
				<div className={style.address}>
					<span style={{ fontWeight: 600 }}> Marina Bay Sands</span>
					<span>10 Bayfront Ave, S018956</span>
				</div>
				<div className={style.map} />
			</div>
			<div id="pa" className={style.goingAndLikes}>
				<div className={style.goings}>
					<div className={style.check} />
					<span className={style.goingText}>{goingNum} going</span>
					{goingAvatars.map(item => {
						return (
							<div
								key={item}
								className={style.avatar}
								style={{ backgroundImage: `url('${item}')` }}
							/>
						);
					})}
				</div>
				<div className={style.likes}>
					<div className={style.like} />
					<span className={style.goingText}>{likeNum} likes</span>
					{likeAvatars.map(item => {
						return (
							<div
								key={item}
								className={style.avatar}
								style={{ backgroundImage: `url('${item}')` }}
							/>
						);
					})}
				</div>
			</div>
			<div>
				{comments.map((comment, index) => {
					return <Comment {...comment} key={index} />;
				})}
			</div>
			{showComment ? (
				<CommentInput
					setShowToast={setShowToast}
					setShowComment={setShowComment}
					onSend={onSend}
				/>
			) : (
				<BottomBar
					setShowComment={setShowComment}
					id={currentIndex}
					{...bottomCbs}
					going={going}
					liked={liked}
				/>
			)}

			<span id="bottom" />
		</div>
	);
}

export default connect(
	function mapStateToProps(state: any) {
		return {
			redirect: state.login.redirect,
			comments: state.detail.comments,
			avatar: state.login.user.avatar,
			eventList: state.home.eventList,
			images: state.detail.images,
			currentIndex: state.detail.currentIndex,
			currentSelect: state.detail.currentSelect,
			goingNum: state.detail.goingNum,
			likeNum: state.detail.likeNum,
			content: state.detail.content,
			showAll: state.detail.showAll,
			goingAvatars: state.detail.goingAvatars,
			likeAvatars: state.detail.likeAvatars
		};
	},
	function mapDispatchToProps(dispatch) {
		return { dispatch };
	}
)(index);
