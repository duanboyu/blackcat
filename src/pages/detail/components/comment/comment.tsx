import * as React from "react";
import * as style from "./style.scss";
export default function comment(props) {
	const { username, avatar, content } = props;

	return (
		<div className={style.comment}>
			<div className={style.commentLeft}>
				<div
					className={style.commentAvatar}
					style={{ backgroundImage: `url('${avatar}')` }}
				/>
			</div>
			<div className={style.commentRight}>
				<div className={style.rightTop}>
					<span className={style.username}>{username}</span>
					<span className={style.commentTime}>9 hours ago</span>
					<div className={style.reply} />
				</div>
				<div className={style.commentContent}>{content}</div>
			</div>
		</div>
	);
}
