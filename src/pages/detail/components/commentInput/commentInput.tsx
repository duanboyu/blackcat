import * as React from "react";
import * as style from "./style.scss";
export default function commentInput(props) {
	const { onSend, setShowComment, setShowToast } = props;
	let inputDOM: HTMLInputElement = null;
	return (
		<div className={style.commentInput}>
			<div className={style.left}>
				<div
					className={style.clear}
					onClick={() => {
						setShowComment(false);
					}}
				/>
				<div className={style.inputWrapper}>
					<input
						ref={input => {
							inputDOM = input;
						}}
						type="text"
					/>
				</div>
			</div>
			<div className={style.right}>
				<div
					className={style.send}
					onClick={() => {
						onSend(inputDOM.value);
						setTimeout(() => {
							setShowToast(false);
						}, 1000);
						inputDOM.value = "";
						setShowToast(true);
					}}
				/>
			</div>
		</div>
	);
}
