import * as React from "react";
import * as style from "./style.scss";
export default function bottomBar(props) {
	const { liked, going, onGoing, onLiked, id, setShowComment } = props;
	return (
		<div className={style.bottomBar}>
			<div className={style.left}>
				<div
					className={style.commentSVG}
					onClick={() => setShowComment(true)}
				/>
				<div
					onClick={() => onLiked(id)}
					className={liked ? style.likeRedSVG : style.likeSVG}
				/>
			</div>
			<div className={style.right}>
				<div
					onClick={() => onGoing(id)}
					className={going ? style.amGoing : style.goingSVG}
				/>
				<span>{going ? "I am going" : "Join"}</span>
			</div>
		</div>
	);
}
