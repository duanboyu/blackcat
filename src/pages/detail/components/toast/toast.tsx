import * as React from "react";
import * as style from "./style.scss";
export default function toast(props) {
	return <div className={style.toast}>{props.content}</div>;
}
