export const searchResult: string;
export const searchTop: string;
export const searchText: string;
export const clearSearch: string;
export const clearText: string;
export const searchBottom: string;
