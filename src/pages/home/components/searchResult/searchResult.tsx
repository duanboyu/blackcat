import * as React from "react";
import * as style from "./style.scss";
import { useMemo } from "react";
export default function searchResult(props) {
	const { resultNum, searchCondition, onClear } = props;
	const { time, channel, from, to } = searchCondition;
	const showText = useMemo(() => {
		let text: string = `Searched for ${searchCondition.channel} Activities`;
		if (time === "ANYTIME") {
			return text;
		}
		if (time === "TOMORROW" || time === "TODAY") {
			return text.concat(` on ${time.toLowerCase()}`);
		}
		if (time === "THIS WEEK" || time === "THIS MONTH") {
			return text.concat(` in ${time.toLowerCase()}`);
		} else {
			if (from != "") {
				text = text.concat(` from ${from.replace(/-/g, "/")}`);
			}

			if (to != "") {
				text = text.concat(` to ${to.replace(/-/g, "/")}`);
			}
			return text;
		}
	}, [time, channel, from, to]);

	return (
		<div className={style.searchResult}>
			<div className={style.searchTop}>
				<span className={style.searchText}>{resultNum} Results</span>
				<div className={style.clearSearch}>
					<div onClick={() => onClear()} className={style.clearText}>
						CLEAR SEARCH
					</div>
				</div>
			</div>
			<div className={style.searchBottom}>{showText}</div>
		</div>
	);
}
