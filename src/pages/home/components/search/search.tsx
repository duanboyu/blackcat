import * as React from "react";
import * as style from "./style.scss";
export default function search(props) {
	const { hintText, submit } = props;
	return (
		<div
			onClick={() => {
				submit();
			}}
			className={hintText ? style.searchWithCondition : style.search}
		>
			<div className={style.searchWrapper}>
				<div className={style.searchMirror} />
				<span className={style.searchText}>SEARCH</span>
			</div>
			<span className={style.searchCondition}>{hintText}</span>
		</div>
	);
}
