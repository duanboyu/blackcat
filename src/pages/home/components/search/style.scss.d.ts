export const search: string;
export const searchWithCondition: string;
export const searchWrapper: string;
export const searchMirror: string;
export const searchText: string;
export const searchCondition: string;
