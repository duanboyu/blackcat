import * as React from "react";
import * as style from "./style.scss";

export default function ChannelPicker(props) {
	const { channel, setChannel, channels } = props;

	return (
		<div className={style.channelPicker}>
			<span className={style.channel}>CHANNEL</span>
			<div className={style.line} />
			<div className={style.items}>
				{channels.map(item => {
					return (
						<div
							onClick={() => {
								setChannel(item);
							}}
							key={item}
							className={
								channel === item ? style.itemWrapperSelect : style.itemWrapper
							}
						>
							<span className={style.item}>{item}</span>
						</div>
					);
				})}
			</div>
		</div>
	);
}
