import * as React from "react";
import * as style from "./style.scss";
import { useMemo, useState } from "react";
import classnames from "classnames";
export default function DatePicker(props) {
	const { time, setTime, setStart, setEnd } = props;
	const dateItems = useMemo(() => {
		return ["ANYTIME", "TODAY", "TOMORROW", "THIS WEEK", "THIS MONTH", "LATER"];
	}, []);
	const [focusOne, setfocusOne] = useState("");

	return (
		<div className={style.datePicker}>
			<span className={style.date}>DATE</span>
			<div className={style.line} />
			<div className={style.filterItems}>
				{dateItems.map(item => {
					const itemClass = classnames(style.ItemWrapper, {
						[style.ItemWrapperSelect]: item === time
					});
					return (
						<div
							onClick={() => {
								setTime(item);
							}}
							className={itemClass}
							key={item}
						>
							<span className={style.filterItem}>{item}</span>
						</div>
					);
				})}
			</div>
			{time == "LATER" ? (
				<div className={style.dateWrapper}>
					<div className={style.tra} />
					<div className={style.right} />
					<input
						type="date"
						className={focusOne === "from" ? style.focus : ""}
						onFocus={() => {
							setfocusOne("from");
						}}
						onChange={e => {
							setStart(e.target.value);
						}}
					/>
					<div className={style.shortline} />
					<div className={style.left} />
					<div className={style.datebox}>
						<input
							onFocus={() => {
								setfocusOne("to");
							}}
							onChange={e => {
								setEnd(e.target.value);
							}}
							className={focusOne == "to" ? style.focus : ""}
							type="date"
						/>
					</div>
				</div>
			) : null}
		</div>
	);
}
