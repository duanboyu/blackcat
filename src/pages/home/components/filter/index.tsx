import * as React from "react";
import * as style from "./style.scss";
import DatePicker from "../datePicker/index";
import ChannelPicker from "../channelPicker/channelPicker";
import Search from "../search/search";
import { useState, useMemo } from "react";

export default function index(props) {
	const { channels, submitSearch } = props;
	const [time, setTime] = useState("");
	const [channel, setChannel] = useState("");
	const [startDate, setStartDate] = useState("");
	const [endDate, setEndDate] = useState("");
	const submit = () => {
		submitSearch({ time, channel, from: startDate, to: endDate });
	};

	const hintText = useMemo(() => {
		if (channel == "" || time == "") {
			return "";
		}
		let timeText = "";
		if (time == "ANYTIME") {
			timeText = "";
		} else if (time == "TODAY" || time == "TOMORROW") {
			timeText = ` on ${time.toLowerCase()}`;
		} else if (time == "LATER") {
			if (startDate != "") {
				timeText = timeText.concat(` from ${startDate.replace(/-/g, "/")}`);

				if (endDate != "") {
					timeText = timeText.concat(` to ${endDate.replace(/-/g, "/")}`);
				}
			}
		} else {
			timeText = ` in ${time.toLocaleLowerCase()}`;
		}
		return `${channel} activities${timeText}`;
	}, [channel, time, startDate, endDate]);

	return (
		<div className={style.searchPage}>
			<div className={style.pickWrapper}>
				<DatePicker
					time={time}
					setTime={setTime}
					setStart={setStartDate}
					setEnd={setEndDate}
				/>
				<ChannelPicker
					channels={channels}
					channel={channel}
					setChannel={setChannel}
				/>
			</div>
			<Search submit={submit} hintText={hintText} />
		</div>
	);
}
