import * as React from "react";
import Header from "../../components/header/Header";
import Card from "../../components/card/Card";
import { connect } from "react-redux";
import SearchResult from "./components/searchResult/searchResult";
import { useEffect, useMemo, useCallback } from "react";
import {
	fetchData,
	setGoing,
	setLiked,
	setShowSearchPinnel,
	setSearchCondition,
	setShowSearchResult,
	loadMoreData,
	setShouldScroll
} from "../../store/home/action";
import { bindActionCreators } from "redux";
import * as style from "./style.scss";
import Filter from "./components/filter/index";
import { SearchCondition } from "src/types";
import NoActivity from "../me/components/noActivity";
import { isResult } from "../../utils";
import { setCurrentIndex } from "../../store/detail/action";
import { setRedirect } from "../../store/login/action";
import { Redirect } from "react-router-dom";

function index(props) {
	const {
		shouldScroll,
		eventList,
		dispatch,
		avatar,
		showSearchPinnel,
		showSearchResult,
		searchCondition,
		redirect,
		location
	} = props;
	if (redirect && redirect != location.pathname) {
		return <Redirect to={redirect} />;
	}
	const { time, channel } = searchCondition;

	useEffect(() => {
		if (!eventList.length) {
			dispatch(fetchData());
		}

		if (shouldScroll) {
			document.documentElement.scrollTop = shouldScroll;
		}

		let scroll = 0;
		const loadDataFun = () => {
			if (
				document.documentElement.scrollTop - scroll > 600 &&
				!window["fobidFetch"]
			) {
				scroll = document.documentElement.scrollTop;
				dispatch(loadMoreData());
			}
		};

		window.addEventListener("scroll", loadDataFun);

		return () => {
			window.removeEventListener("scroll", loadDataFun);
		};
	}, []);

	const linkToMe = useCallback(() => {
		dispatch(setRedirect("/me"));
	}, []);
	const submitSearch = useCallback((searchCondition: SearchCondition) => {
		dispatch(setSearchCondition(searchCondition));
		dispatch(setShowSearchPinnel(false));
		dispatch(setShowSearchResult(true));
	}, []);

	const showPinnelCb = useCallback(() => {
		dispatch(setShowSearchPinnel(true));
	}, []);
	const onClear = useCallback(() => {
		dispatch(setShowSearchResult(false));
	}, []);
	const channels = useMemo(() => {
		const channels = new Set();
		eventList.forEach(event => {
			channels.add(event.channel);
		});
		const chanArr = Array.from(channels);
		chanArr.unshift("All");
		return chanArr;
	}, [eventList]);

	const cardCbs = useMemo(() => {
		return bindActionCreators(
			{
				onGoing: setGoing,
				onLiked: setLiked,
				setIndex: setCurrentIndex,
				toDetail: setRedirect,
				setShouldScroll: setShouldScroll
			},
			dispatch
		);
	}, []);

	const showList = useMemo(() => {
		if (!showSearchResult) {
			return eventList.map((item, index) => {
				return <Card {...cardCbs} id={index} key={index} {...item} />;
			});
		} else {
			const resultList = [];
			eventList.forEach((item, index) => {
				if (channel == "All" || channel === item.channel) {
					if (time == "ANYTIME" || isResult(time, item.time)) {
						const cardJSX = (
							<Card {...cardCbs} id={index} key={index} {...item} />
						);
						resultList.push(cardJSX);
					}
				}
			});
			return resultList;
		}
	}, [eventList, showSearchResult, searchCondition]);

	return (
		<div className={showSearchPinnel ? style.homeWrapper : ""}>
			{showSearchPinnel ? (
				<Filter submitSearch={submitSearch} channels={channels} />
			) : null}

			<div>
				<Header
					rightCallback={linkToMe}
					leftCallback={showPinnelCb}
					showSearch={true}
					avatar={avatar}
				/>
				{showSearchResult ? (
					<SearchResult
						onClear={onClear}
						resultNum={showList.length}
						searchCondition={searchCondition}
					/>
				) : null}
				{showSearchResult && !showList.length ? (
					<NoActivity showOnHome={true} />
				) : (
					<div>{showList}</div>
				)}
			</div>
		</div>
	);
}

export default connect(
	function mapStateToProps(state: any) {
		return {
			redirect: state.login.redirect,
			eventList: state.home.eventList,
			avatar: state.login.user.avatar,
			showSearchPinnel: state.home.showSearchPinnel,
			showSearchResult: state.home.showResult,
			searchCondition: state.home.searchCondition,
			shouldScroll: state.home.shouldScroll
		};
	},
	function mapStateToProps(dispatch: any) {
		return { dispatch };
	}
)(index);
