import * as React from "react";
import { connect } from "react-redux";
import { useCallback, memo, useState, useMemo } from "react";
import Header from "../../components/header/Header";
import * as style from "./style.scss";
import { EventTag } from "../../types";
import Card from "../../components/card/Card";
import NoActivity from "./components/noActivity";
import { setGoing, setLiked } from "../../store/home/action";
import { bindActionCreators } from "redux";
import { Redirect } from "react-router-dom";
import { setRedirect } from "../../store/login/action";
const Me = props => {
	const {
		avatar,
		username,
		email,
		eventList,
		dispatch,
		redirect,
		location
	} = props;
	if (redirect && redirect != location.pathname) {
		return <Redirect to={redirect} />;
	}
	const cardCbs = useMemo(() => {
		return bindActionCreators(
			{
				onGoing: setGoing,
				onLiked: setLiked
			},
			dispatch
		);
	}, []);

	const [nowLike, setNowLike] = useState(true);
	const [nowGoing, setNowGoing] = useState(false);
	const [nowPast, setNowPast] = useState(false);
	let likes: number = 0;
	let goings: number = 0;
	function generateJSX(eventList: Array<EventTag>) {
		const likeList = [];
		const goingList = [];
		eventList.forEach((event, index) => {
			if (event.liked) {
				const card = (
					<Card id={index} {...cardCbs} key={event.title} {...event} />
				);
				likeList.push(card);
				likes++;
			}
			if (event.going) {
				const card = (
					<Card id={index} {...cardCbs} key={event.title} {...event} />
				);
				goingList.push(card);
				goings++;
			}
		});
		if (nowLike) {
			if (likes == 0) {
				return <NoActivity />;
			}
			return likeList;
		}
		if (nowGoing) {
			if (goings == 0) {
				return <NoActivity />;
			}
			return goingList;
		}
		if (nowPast) {
			return <NoActivity />;
		}
	}
	function setStatus(status: string) {
		switch (status) {
			case "like":
				setNowLike(true);
				setNowGoing(false);
				setNowPast(false);
				break;
			case "going":
				setNowLike(false);
				setNowGoing(true);
				setNowPast(false);
				break;
			case "past":
				setNowLike(false);
				setNowGoing(false);
				setNowPast(true);
		}
	}

	const goback = useCallback(() => {
		dispatch(setRedirect("/home"));
	}, []);

	const JSX = generateJSX(eventList);
	return (
		<div className={style.mePage}>
			<Header avatar={avatar} showSearch={false} leftCallback={goback} />

			<div className={style.userInfo}>
				<div className={style.basicInfo}>
					<div
						className={style.avatar}
						style={{ backgroundImage: `url("${avatar}")` }}
					/>
					<div className={style.username}>{username}</div>
					<div className={style.emailWrapper}>
						<div className={style.emailBox} />
						<span className={style.email}>{email} </span>
					</div>
				</div>
				<div className={style.otherInfo}>
					<div className={style.infoWrapper}>
						<div className={style.leftDivide} />
						<div className={style.rightDivide} />
						<div
							className={style.oneInfo}
							onClick={() => {
								setStatus("like");
							}}
						>
							<div className={nowLike ? style.greenLike : style.brownLike} />
							<div className={nowLike ? style.greenText : style.brownText}>
								{likes} likes
							</div>
						</div>
						<div
							className={style.oneInfo}
							onClick={() => {
								setStatus("going");
							}}
						>
							<div className={nowGoing ? style.greenGoing : style.brownGoing} />
							<div className={nowGoing ? style.greenText : style.brownText}>
								{goings} going
							</div>
						</div>
						<div
							className={style.oneInfo}
							onClick={() => {
								setStatus("past");
							}}
						>
							<div className={nowPast ? style.greenPast : style.brownPast} />
							<div className={nowPast ? style.greenText : style.brownText}>
								0 past
							</div>
						</div>
					</div>
				</div>
			</div>

			{JSX}
		</div>
	);
};

export default connect(
	function mapStateToProps(state: any) {
		return {
			avatar: state.login.user.avatar,
			username: state.login.user.username,
			email: state.login.user.email,
			eventList: state.home.eventList,
			redirect: state.login.redirect
		};
	},
	function mapDispatchtoProps(dispatch: any) {
		return { dispatch };
	}
)(memo(Me));
