import * as React from "react";
import * as style from "./style.scss";
export default function noActivity(props) {
	const show = props.showOnHome;

	return (
		<div className={show ? style.showOnHome : style.noAc}>
			<div className={style.ball} />
			<span className={style.noAcText}>No activity found</span>
		</div>
	);
}
