import * as React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import { setUser, setRedirect } from "../store/login/action";

function Auth(props) {
	const { location, username, dispatch } = props;
	const pageArray = ["/login", "/home", "/me", "/detail"];
	if (pageArray.includes(location.pathname)) {
		if (location.pathname === "/login" || username) {
			return null;
		}
		console.log("开始验证");
		axios.get("/api/verify").then(res => {
			if (res.data.logined) {
				console.log("成功");

				dispatch(setUser(res.data.user));
				dispatch(setRedirect("/home"));
			} else {
				console.log("失败");
				dispatch(setRedirect("/login"));
			}
		});
		return null;
	}
	return <Redirect to="/login" />;
}

export default connect(
	function mapstateToProps(state: any) {
		return {
			username: state.login.user.username
		};
	},

	function mapDispatchToProps(dispatch) {
		return {
			dispatch
		};
	}
)(Auth);
