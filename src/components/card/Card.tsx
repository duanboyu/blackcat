import * as React from "react";
import { memo } from "react";
import * as style from "./CardStyle.scss";
import {} from "../../types";
import { withRouter } from "react-router-dom";

function Card(props) {
	const {
		id,
		setShouldScroll,
		onGoing,
		onLiked,
		setIndex,
		toDetail,
		username,
		channel,
		time,
		content,
		title,
		goingNum,
		likesNum,
		avatar,
		img,
		going,
		liked
	} = props;

	const date = new Date(Number(time));
	const months = [
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"May",
		"Jun",
		"Jul",
		"Aug",
		"Sep",
		"Oct",
		"Nov",
		"Dec"
	];
	const dataString = `${date.getDate()} ${
		months[date.getMonth()]
	} ${date.getFullYear()}`;
	const goingClass = going ? style.checked : style.check;
	const goingText = going ? "I am going!" : goingNum + " Going";
	const likeClass = liked ? style.liked : style.heart;
	const likeText = liked ? "I like it" : likesNum + " Likes";

	return (
		<div className={style.cardWrapper}>
			<div className={style.card}>
				<div className={style.top}>
					<div className={style.topLeft}>
						<div
							className={style.avatar}
							style={{ backgroundImage: `url("${avatar}")` }}
						/>
						<span className={style.username}>{username}</span>
					</div>
					<div className={style.channelName}>{channel}</div>
				</div>
				<div
					onClick={() => {
						setShouldScroll(document.documentElement.scrollTop);
						document.documentElement.scrollTop = 0;
						setIndex(id);
						toDetail("/detail");
					}}
					className={style.titleWrapper}
				>
					<div className={style.titleLeft}>
						<div className={style.title}>{title}</div>
						<div className={style.titleTimeWrapper}>
							<div className={style.titleClock} />
							<div className={style.tilteTime}>{dataString}</div>
						</div>
					</div>
					<div
						className={style.titleImage}
						style={{ backgroundImage: `url("${img}")` }}
					/>
				</div>
				<div className={style.content}>{content}</div>
				<div className={style.status}>
					<div className={style.goingWrapper}>
						<div onClick={() => onGoing(id)} className={goingClass} />
						<span className={style.going}> {goingText}</span>
					</div>
					<div className={style.likeWrapper}>
						<div
							onClick={() => {
								onLiked(id);
							}}
							className={likeClass}
						/>
						<div className={style.going}>{likeText}</div>
					</div>
				</div>
			</div>
			<div className={style.border} />
		</div>
	);
}

export default withRouter(memo(Card));
