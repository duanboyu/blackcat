import axios from "axios";
export const SET_REDIRECT = "SET_REDIRECT";
export const ACTION_SET_USER = "SET_USERNAME";
import { Action, User } from "../../types";

export function loginSubmit(username: string, password: string) {
	return (dispatch: any, getState: any) => {
		axios.post("/api/login", { username, password }).then(function(res) {
			if (res.data.msg == "success") {
				dispatch(setUser(res.data.user));
				dispatch(setRedirect("/home"));
			}
		});
	};
}
export function setRedirect(redirect: string): Action {
	return {
		type: SET_REDIRECT,
		payload: redirect
	};
}
export function setUser(user: User): Action {
	return {
		type: ACTION_SET_USER,
		payload: user
	};
}
