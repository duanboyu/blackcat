import { Action } from "../../types";
import { ACTION_SET_USER, SET_REDIRECT } from "./action";

import { loginState } from "../../types";

const defaultState: loginState = {
	user: {},
	redirect: ""
};
function userReducer(state = defaultState, action: Action) {
	const { type, payload } = action;
	switch (type) {
		case ACTION_SET_USER:
			return { user: payload };
		case SET_REDIRECT:
			return {
				...state,
				redirect: payload
			};
	}

	return state;
}
export default userReducer;
