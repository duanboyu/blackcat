import { createStore, combineReducers, applyMiddleware } from "redux";
import loginReducer from "./login/reducer";
import homeReducer from "./home/reducer";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import detailReducer from "./detail/reducer";

const reducers = {
	login: loginReducer,
	home: homeReducer,
	detail: detailReducer
};

export default createStore(
	combineReducers(reducers),
	composeWithDevTools(applyMiddleware(thunk))
);
