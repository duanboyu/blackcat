import { Action } from "../../types";
import {
	SET_EVENT_LISTS,
	SET_GOING,
	SET_LIKED,
	SET_SHOW_SEARCHPINNEL,
	SET_SHOW_SEARCH_RESULT,
	SET_SEARCH_CONDITION,
	UPDATE_DATA,
	SET_SHOULD_SCROLL
} from "./action";

import { homeState } from "../../types";

const defaultState: homeState = {
	eventList: [],
	showSearchPinnel: false,
	showResult: false,
	shouldScroll: 0,
	searchCondition: {
		time: "",
		channel: ""
	}
};

function homeReducer(state = defaultState, action: Action) {
	const { type, payload } = action;
	switch (type) {
		case SET_SHOULD_SCROLL:
			return { ...state, shouldScroll: payload };
		case SET_EVENT_LISTS:
			return { ...state, eventList: payload };
		case SET_SHOW_SEARCHPINNEL:
			return { ...state, showSearchPinnel: payload };
		case SET_SHOW_SEARCH_RESULT:
			if (payload) {
				window["fobidFetch"] = true;
			} else {
				window["fobidFetch"] = false;
			}
			return { ...state, showResult: payload };
		case SET_GOING:
			const newEventList = state.eventList.slice(0);
			if (!newEventList[payload].going) {
				newEventList[payload].going = true;

				newEventList[payload].goingNum = newEventList[payload].goingNum + 1;
				return { ...state, eventList: newEventList };
			}
			return state;

		case SET_LIKED:
			const likedNewEventList = state.eventList.slice(0);
			if (!likedNewEventList[payload].liked) {
				likedNewEventList[payload].liked = true;

				likedNewEventList[payload].likesNum =
					likedNewEventList[payload].likesNum + 1;
				return { ...state, eventList: likedNewEventList };
			}
			return state;
		case SET_SEARCH_CONDITION:
			return { ...state, searchCondition: payload };
		case UPDATE_DATA:
			const newList = state.eventList;
			return { ...state, eventList: newList.concat(payload) };
	}

	return state;
}
export default homeReducer;
