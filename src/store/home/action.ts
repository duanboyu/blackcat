import { Action, EventTag, SearchCondition } from "../../types";
import axios from "axios";

export const SET_SHOW_SEARCH_RESULT = "SHOW_SEARCH_RESULT";
export const SET_SHOW_SEARCHPINNEL = " SET_SHOW_SEARCHPINNEL";
export const SET_EVENT_LISTS = "LOAD_EVENT_LISTS";
export const SET_GOING = "SET_GOING";
export const SET_LIKED = "SET_LIKED";
export const SET_SEARCH_CONDITION = "SET_SEARCH_CONDITION";
export const SET_SHOULD_SCROLL = "SET_SHOULD_SCROLL";
export const UPDATE_DATA = "UPDATE_DATA";
export function setEvent(eventList: Array<EventTag>): Action {
	return {
		type: SET_EVENT_LISTS,
		payload: eventList
	};
}

export function setGoing(index: number): Action {
	return {
		type: SET_GOING,
		payload: index
	};
}

export function setLiked(index: number): Action {
	return {
		type: SET_LIKED,
		payload: index
	};
}
export function setShouldScroll(shouldScroll: number): Action {
	return {
		type: SET_SHOULD_SCROLL,
		payload: shouldScroll
	};
}
export function setShowSearchResult(ifShowSR: boolean): Action {
	return {
		type: SET_SHOW_SEARCH_RESULT,
		payload: ifShowSR
	};
}
export function updateData(eventList: Array<EventTag>): Action {
	return {
		type: UPDATE_DATA,
		payload: eventList
	};
}

export function loadMoreData() {
	return dispatch => {
		axios.get("/api/eventList").then(res => {
			dispatch(updateData(res.data));
		});
	};
}
export function setSearchCondition(searchCondition: SearchCondition): Action {
	return {
		type: SET_SEARCH_CONDITION,
		payload: searchCondition
	};
}
export function setShowSearchPinnel(ifShowSP: boolean): Action {
	return {
		type: SET_SHOW_SEARCHPINNEL,
		payload: ifShowSP
	};
}
export function fetchData() {
	return dispatch => {
		axios.get("/api//eventList").then(res => {
			dispatch(setEvent(res.data));
		});
	};
}
