import { DetailState, Action } from "../../types";
import {
	SET_DETAIL_DATA,
	SET_CURRENT_INDEX,
	SET_SHOW_ALL,
	SET_CURRENT_SELECT,
	SEND_COMMENT
} from "./action";

const defaultState: DetailState = {
	currentIndex: 0,
	currentSelect: "Details",
	images: [],
	showAll: false,
	goingNum: 0,
	likeNum: 0,
	content: "",
	goingAvatars: [],
	likeAvatars: [],
	comments: []
};

function detailReducer(state = defaultState, action: Action): DetailState {
	const { type, payload } = action;
	switch (type) {
		case SET_CURRENT_SELECT:
			return { ...state, currentSelect: payload };
		case SET_DETAIL_DATA:
			if (payload.content.length > 150) {
				return {
					...state,
					images: payload.images,
					goingNum: payload.goingNum,
					likeNum: payload.likeNum,
					goingAvatars: payload.goingAvatars,
					likeAvatars: payload.likeAvatars,
					content: payload.content,
					comments: payload.comments
				};
			} else {
				return {
					...state,
					comments: payload.comments,
					images: payload.images,
					goingNum: payload.goingNum,
					likeNum: payload.likeNum,
					goingAvatars: payload.goingAvatars,
					likeAvatars: payload.likeAvatars,
					content: payload.content,
					showAll: true
				};
			}
		case SEND_COMMENT:
			const newComments = state.comments.slice(0);
			const newComment = {
				avatar: payload.avatar,
				username: payload.username,
				content: payload.content
			};
			console.log(newComment);
			newComments.push(newComment);
			return { ...state, comments: newComments };
		case SET_CURRENT_INDEX:
			return {
				...state,
				currentIndex: payload
			};
		case SET_SHOW_ALL:
			return {
				...state,
				showAll: payload
			};
	}
	return state;
}

export default detailReducer;
