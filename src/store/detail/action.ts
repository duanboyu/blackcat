import { Action, Comment } from "../../types";
import axios from "axios";

export const SET_CURRENT_INDEX = " SET_CURRENT_INDEX";
export const SET_CURRENT_SELECT = " CURRENT_SELECT";
export const SET_DETAIL_DATA = "SET_DETAIL_DATA";
export const SET_SHOW_ALL = "SET_SHOW_ALL";
export const SEND_COMMENT = "SEND_COMMENT";

export const setCurrentIndex = (index: number): Action => {
	return {
		type: SET_CURRENT_INDEX,
		payload: index
	};
};

export const setCurrentSelect = (currentSelect: string): Action => {
	return {
		type: SET_CURRENT_SELECT,
		payload: currentSelect
	};
};

export const setDetailData = (detailData: any): Action => {
	return {
		type: SET_DETAIL_DATA,
		payload: detailData
	};
};

export const setShowAll = (showAll: boolean): Action => {
	return {
		type: SET_SHOW_ALL,
		payload: showAll
	};
};

export const fetchDetailData = () => {
	return dispatch => {
		axios.get("/api/detail").then(res => {
			dispatch(setDetailData(res.data));
		});
	};
};

export const sendComment = (comment: Comment): Action => {
	return { type: SEND_COMMENT, payload: comment };
};
