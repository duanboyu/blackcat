import * as React from "react";
import { render } from "react-dom";
import "./normalize.css";
import App from "./App";
import { Provider } from "react-redux";
import store from "./store/store";

render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("app")
);
