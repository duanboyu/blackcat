export function isResult(
	condition: string,
	giveTime: string,
	from?: string,
	to?: string
): boolean {
	const todayZeroTime = getZeroTime(new Date());

	const tomorrowDate = new Date();

	tomorrowDate.setDate(tomorrowDate.getDate() + 1);

	const tomorrowZeroTime = getZeroTime(tomorrowDate);

	const dayAfterTomorrow = new Date();

	dayAfterTomorrow.setDate(dayAfterTomorrow.getDate() + 2);

	const dayAfterTomorrowZeroTime = getZeroTime(dayAfterTomorrow);
	const dateObj = new Date();
	switch (condition) {
		case "ANYTIME":
			return true;
		case "TODAY":
			if (
				Number(giveTime) > todayZeroTime &&
				Number(giveTime) < tomorrowZeroTime
			) {
				return true;
			} else {
				return false;
			}

		case "TOMORROW":
			if (
				Number(giveTime) > tomorrowZeroTime &&
				Number(giveTime) < dayAfterTomorrowZeroTime
			) {
				console.log(true);
				return true;
			} else {
				console.log(false);
				return false;
			}
		case "THIS WEEK":
			const todayDate = dateObj.getDate();
			const todayDay = dateObj.getDay();
			const backdays = todayDay == 0 ? 6 : todayDay - 1;
			dateObj.setDate(todayDate - backdays);
			const weekStart = dateObj.getTime();
			dateObj.setDate(dateObj.getDate() + 7);
			const weekEnd = dateObj.getTime();

			if (Number(giveTime) > weekStart && Number(giveTime) < weekEnd) {
				return true;
			} else {
				return false;
			}
		case "THIS MONTH":
			dateObj.setDate(1);
			setZero(dateObj);
			const monthStart = dateObj.getTime();
			dateObj.setMonth(dateObj.getMonth() + 1);
			const monthend = dateObj.getTime();
			if (Number(giveTime) > monthStart && Number(giveTime) < monthend) {
				return true;
			} else {
				return false;
			}
	}

	return true;
}

function getZeroTime(date: Date): number {
	date.setHours(0);
	date.setMinutes(0);
	date.setMilliseconds(0);
	return Number(date.getTime());
}

function setZero(date: Date): void {
	date.setHours(0);
	date.setMinutes(0);
	date.setMilliseconds(0);
}

export function generateDateString(date: number): string {
	const months = [
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"May",
		"Jun",
		"Jul",
		"Aug",
		"Sep",
		"Oct",
		"Nov",
		"Dec"
	];
	const dateObj = new Date(date);
	return `${dateObj.getDate()} ${
		months[dateObj.getMonth()]
	} ${dateObj.getFullYear()}`;
}
