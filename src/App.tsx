import * as React from "react";
import Login from "./pages/login/Login";
import Home from "./pages/home/index";
import * as AppStyle from "./App.scss";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Auth from "./components/Auth";
import Me from "./pages/me/index";
import Detail from "./pages/detail/index";
const App = () => {
	return (
		<div className={AppStyle.app}>
			<Router>
				<Route path="/" component={Auth} />
				<Route path="/login" component={Login} exact />
				<Route path="/home" component={Home} exact />
				<Route path="/me" component={Me} exact />
				<Route path="/detail" component={Detail} exact />
			</Router>
		</div>
	);
};

export default App;
