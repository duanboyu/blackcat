export interface Action {
	type: string;
	payload?: any;
}

export interface loginState {
	user: User;
	redirect: string;
}

export interface homeState {
	eventList: Array<EventTag>;
	showSearchPinnel: boolean;
	showResult: boolean;
	shouldScroll: number;
	searchCondition: SearchCondition;
}

export interface SearchCondition {
	channel: string;
	time: string;
	from?: string;
	to?: string;
}
export interface EventTag {
	username: string;
	channel: string;
	time: string;
	content: string;
	goingNum: any;
	likesNum: any;
	img: string;
	title: string;
	avatar: string;
	going: boolean;
	liked: boolean;
}

export interface User {
	username?: string;
	avatar?: string;
	email?: string;
}

export interface Comment {
	avatar: string;
	username: string;
	content: string;
}
export interface DetailState {
	content: string;
	currentIndex: number;
	currentSelect: string;
	images: Array<string>;
	showAll: boolean;
	goingNum: number;
	likeNum: number;
	goingAvatars: Array<string>;
	likeAvatars: Array<string>;
	comments: Array<Comment>;
}
